package com.kytms.customerFile.service;

import com.kytms.core.model.CommModel;
import com.kytms.core.model.JgGridListModel;
import com.kytms.core.service.BaseService;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
   客户档案服务层接口
 * 陈小龙
 * @create 2018-1-5
 */
public interface CustomerService<Customer> extends BaseService<Customer> {
    JgGridListModel getList(CommModel commModel);
}
